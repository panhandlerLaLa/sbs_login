package kr.pe.lala.edu.snsapp;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Administrator on 2017-09-17.
 */

public abstract class FragmentBase extends Fragment implements Runnable{
    private int layoutId;

    public void setLayoutId(int layoutId){
        this.layoutId = layoutId;
    }

    public MainActivity getMainActivity(){
        return (MainActivity)getActivity();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(layoutId, container, false);
    }

    private String url;
    private String param;

    public void executeHttp(String url, String param){
        this.url = url;
        this.param = param;

        Thread t = new Thread(this);
        t.start();
    }

    @Override
    public void run() {
        try {
            URL url = new URL(this.url);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(3000);
            conn.setConnectTimeout(3000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-type", "application/x-www-form-urlencoded");
            conn.connect();

            OutputStream os = conn.getOutputStream();
            os.write(param.getBytes("UTF-8"));
            os.close();

            InputStream is = conn.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            String result = "";
            String line = "";

            while((line = br.readLine())!=null){
                result = result + line + "\n";
            }

            JSONObject obj = new JSONObject(result);
            Message msg = new Message();
            msg.obj = obj;

            handler.sendMessage(msg);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    Handler handler = new Handler(){
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            onJSONResponse((JSONObject)msg.obj);
        }
    };

    public abstract void onJSONResponse(JSONObject obj);
}