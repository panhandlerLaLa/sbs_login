package kr.pe.lala.edu.snsapp;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    private long loginToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        changeFragment(new LoginFragment());
    }

    public void changeFragment(Fragment fragment){
        getSupportFragmentManager().beginTransaction().replace(R.id.parent, fragment).commit();
    }

    public void setLoginToken(long token){
        this.loginToken = token;
    }

    public long getLoginToken(){
        return this.loginToken;
    }
}
