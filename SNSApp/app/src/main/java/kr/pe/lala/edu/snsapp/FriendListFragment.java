package kr.pe.lala.edu.snsapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Administrator on 2017-09-17.
 */

public class FriendListFragment extends FragmentBase{

    ListView lvFriendList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayoutId(R.layout.layout_friendlist);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lvFriendList = (ListView) view.findViewById(R.id.lvFriends);

        String param = "command=listFriends&token="+getMainActivity().getLoginToken();
        executeHttp("http://lala-edu.oa.gg/network_example5.php", param);
    }

    @Override
    public void onJSONResponse(JSONObject obj) {
        try {
            boolean result = obj.getBoolean("result");
            if(result){
                lvFriendList.setAdapter(createAdapter(obj.getJSONArray("data")));
                return;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Toast.makeText(getContext(), "Login Token is Wrong", Toast.LENGTH_LONG).show();
    }

    public BaseAdapter createAdapter(final JSONArray arr){
        return new BaseAdapter() {
            public int getCount() { return arr.length(); }
            public Object getItem(int position) {
                try {
                    return arr.get(position);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return null;
            }
            public long getItemId(int position) { return position; }
            public View getView(int position, View convertView, ViewGroup parent) {
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                View v = inflater.inflate(R.layout.item_friend, null, false);

                TextView tvId = (TextView) v.findViewById(R.id.tvId);
                TextView tvName = (TextView) v.findViewById(R.id.tvName);
                try {
                    JSONObject obj = arr.getJSONObject(position);
                    tvId.setText(obj.getString("ID"));
                    tvName.setText(obj.getString("NAME"));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                return v;
            }
        };
    }
}
