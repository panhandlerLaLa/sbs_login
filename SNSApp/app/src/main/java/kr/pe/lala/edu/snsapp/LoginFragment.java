package kr.pe.lala.edu.snsapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Administrator on 2017-09-17.
 */

public class LoginFragment extends FragmentBase implements View.OnClickListener{

    EditText etId, etPassword;
    Button btnJoin, btnLogin;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayoutId(R.layout.layout_login);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        etId = (EditText) view.findViewById(R.id.etId);
        etPassword = (EditText) view.findViewById(R.id.etPassword);
        btnJoin = (Button) view.findViewById(R.id.btnJoin);
        btnLogin = (Button) view.findViewById(R.id.btnLogin);

        btnJoin.setOnClickListener(this);
        btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnJoin: executeJoin(); break;
            case R.id.btnLogin: executeLogin(); break;
        }
    }

    private void executeJoin(){
        getMainActivity().changeFragment(new JoinFragment());
    }

    private void executeLogin(){
        String id = etId.getText().toString();
        String password = etPassword.getText().toString();

        String param = "command=login&id="+id+"&password="+password;
        executeHttp("http://lala-edu.oa.gg/network_example5.php", param);
    }

    @Override
    public void onJSONResponse(JSONObject obj) {
        Log.i("result", obj.toString());
        try {
            boolean result = obj.getBoolean("result");
            if(result) {
                getMainActivity().setLoginToken(obj.getJSONObject("data").getLong("token"));
                getMainActivity().changeFragment(new FriendListFragment());
                return;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Toast.makeText(getActivity(), "Your id or password is wrong", Toast.LENGTH_LONG).show();
    }
}
