package kr.pe.lala.edu.snsapp;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONObject;

/**
 * Created by Administrator on 2017-09-17.
 */

public class JoinFragment extends FragmentBase implements View.OnClickListener{

    EditText etId, etPassword, etRePassword;
    Button btnJoin, btnCancel;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setLayoutId(R.layout.layout_join);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        etId = (EditText) view.findViewById(R.id.etId);
        etPassword = (EditText) view.findViewById(R.id.etPassword);
        etRePassword = (EditText) view.findViewById(R.id.etRePassword);
        btnJoin = (Button) view.findViewById(R.id.btnJoin);
        btnCancel = (Button) view.findViewById(R.id.btnCancel);

        btnJoin.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnJoin: executeJoin(); break;
            case R.id.btnCancel: executeCancel(); break;
        }
    }

    private void executeJoin(){

    }

    private void executeCancel(){
        getMainActivity().changeFragment(new LoginFragment());
    }

    @Override
    public void onJSONResponse(JSONObject obj) {

    }
}